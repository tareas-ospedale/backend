# backend
El presente proyecto se realiza con el fin de demostrar mis conocimentos en Laravel, Vuejs y MySql como herramientas principales para el desarrollo web.
El proyecto es trabajado con Laravel 8.4, Vue 2.6, nodejs 15.10, npm 7.11, Bootstrap 4.5.
Fue desarrollado en Ubuntu 20.04 con PHP 7.4 y LAMPP focal en Amazon Web Services EC2 el cual tiene asociada una base de datos MySql en el puerto 3306. 

	
El proyecto consta de un login, un CRUD de usuarios y un CRUD de tareas. 

La principal vista del sistema es un login, que luego de acceder redirige a la sección de tareas, donde se pueden realizar las siguientes acciones:

1.	Ver todas las tareas con los diferentes filtros:
  **NOTA:** Los filtros son excluyentes. Ejemplo: Si selecciona el **filtrar por estado no finalizada** y **a su vez por tareas creadas por el usuario actual**, solo traerá las tareas que cumplan con ambas condiciones.

2.	Crear una tarea con los siguientes datos
  a.	Nombre
  b.	Descripción
  c.	¿Fue finalizada? Marcar el check en caso de que sí
  d.	Fecha de vencimiento

3.	Editar una tarea con los siguientes datos
  a.	Nombre
  b.	Descripción
  c.	¿Fue finalizada? Marcar el check en caso de que sí
  d.	Fecha de vencimiento
  **NOTA:** La edición solo puede ser ejecutada por el mismo usuario que realizó la creación de la tarea.

4.	Eliminar una tarea. Esta tarea solo puede ser eliminada por el mismo usuario que la creó.
Módulo de usuarios

Este módulo **solo puede ser accedido por usuarios con roles de administrador**.
En este módulo se podrá realizar la gestión de usuarios, tales como listar, crear, editar y eliminar.
1.	El listado de usuarios se ejecuta en el momento que se ingresa al modulo

2.	Crear un usuario con los siguientes datos:
  a.	Nombre
  b.	Correo (debe de ser único en el sistema)
  c.	Contraseña
  d.	Confirmar contraseña
  e.	Rol (Creador de contenido o administrador)
Después de llenar exitosamente esta información, el usuario puede tener acceso a la plataforma según el role escogido:

3.	Editar un usuario con los siguientes datos:
  a.	Nombre
  b.	Correo
  c.	Rol
  
4.	Eliminar un usuario. El usuario que está actualmente no logueado NO puede eliminarse a sí mismo del sistema.

**Roles:** actualmente existen dos roles, uno que se llama Creador de contenido y administrador. 
**El creador de contenido** puede entrar al sistema y puede ver tareas, crear una tarea, editar (solo las tareas propias) y eliminar (solo las tareas propias).
**El administrador** tiene acceso al modulo de usuarios y tiene control total sobre estos; sin embargo, con las tareas tiene los mismos permisos que un creador de contenido, es decir, solo puede editar y eliminar sus propias tareas.
Validaciones: En los diferentes formularios se encuentran validaciones que se realizan en el controlador para verificar que cumplan con los requerimientos mínimos, dichas validaciones aparecen a medida que un campo no sea diligenciado correctamente. Estas validaciones están presentes en crear y editar tarea como también en crear y editar usuario.
