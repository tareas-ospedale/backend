<?php

namespace App\Http\Controllers;

use App\Models\Tarea;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\TareaRequest;

class TareaController extends Controller
{
    public function __construct()
    {
      $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      /*$per_page = $request->per_page;
      $finalizada = $request->finalizada;
      $mis_tareas = $request->mis_tareas;
      if(trim($finalizada)!="" && $finalizada < 2)
        $tareas = Tarea::where("finalizada",$finalizada);
      if($mis_tareas=="true") {
        $tareas = isset($tareas) ? $tareas->where("user_id", Auth::user()->id) : Tarea::where("user_id", Auth::user()->id);
      }
      if(!isset($tareas))
        $tareas = Tarea::paginate($per_page);
      else
        $tareas = $tareas->paginate($per_page);

      return $tareas;*/
      return view('tareas.index')->with(array("user_id" => Auth::user()->id));
    }

    public function consultar(Request $request){
      $per_page = $request->per_page;
      $finalizada = $request->finalizada;
      $mis_tareas = $request->mis_tareas;
      if(trim($finalizada)!="" && $finalizada < 2)
        $tareas = Tarea::where("finalizada",$finalizada);
      if(trim($mis_tareas)!="" && $mis_tareas==1) {
        $tareas = isset($tareas) ? $tareas->where("user_id", Auth::user()->id) : Tarea::where("user_id", Auth::user()->id);
      }
      if(!isset($tareas))
        $tareas = Tarea::orderBy("fecha_vencimiento","asc")->paginate($per_page);
      else
        $tareas = $tareas->orderBy("fecha_vencimiento","asc")->paginate($per_page);

      return ["tareas" => $tareas, "usuarios" => User::select("id","name")->get()];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //if(isset(Auth::user()->id))

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function crear(TareaRequest $request)
    {
      $tarea = new Tarea();
      $tarea->nombre = $request->input("nombre");
      $tarea->descripcion = $request->input("descripcion");
      $tarea->finalizada = $request->input("finalizada") != "" ? $request->input("finalizada") : 0;
      $tarea->fecha_vencimiento = $request->input("fecha_vencimiento");
      $tarea->user_id = Auth::user()->id;
      return $tarea->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tarea $tarea)
    {
        return $tarea->nombre;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tarea $tarea, Request $request)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actualizar(TareaRequest $request, $tarea_id)
    {
      $tarea = Tarea::find($tarea_id);
      $tarea->nombre = $request->input("nombre");
      $tarea->descripcion = $request->input("descripcion");
      $tarea->finalizada = $request->input("finalizada") != "" ? $request->input("finalizada") : 0;
      $tarea->fecha_vencimiento = $request->input("fecha_vencimiento");
      $tarea->user_id = Auth::user()->id;
      return $tarea->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tarea $tarea)
    {
      //$tarea->delete();
    }

    public function borrar($tarea_id){
      if(!Auth::guest()) {
        $tarea = Tarea::find($tarea_id);
        if(Auth::user()->id == $tarea->user->id)
          return $tarea->delete();
      }
    }
}
