<?php
namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenBlacklistedException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use App\Http\Requests\UserRequest;
use Illuminate\Validation\Rule;


class UserController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

  public function index(Request $request){
    return view('users.index');
  }

  public function consultar(Request $request, $problema_controlador=0){
    $per_page = $request->per_page;
    $usuarios = User::orderBy("created_at","desc")->paginate($per_page);
    return $usuarios;
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function actualizarUsuario(/*UserRequest*/ Request $request, $usuario_id)
  {
    $data = request()->validate([
      'name' => 'required',
      'email' => ['required', 'email', Rule::unique('users')->ignore($usuario_id)],
    ],
    [
      'email.unique' => 'El correo electrónico ya existe en el sistema.'
    ]);
    $usuario = User::find($usuario_id);
    $usuario->name = $request->input("name");
    $usuario->email = $request->input("email");
    return $usuario->save();
  }

  public function borrar($usuario_id){
    if(!Auth::guest()&&Auth::user()->esAdministrador()) {
      $usuario = User::find($usuario_id);
       return $usuario->delete();
    }
  }

  public function authenticate(Request $request)
  {
    $credentials = $request->only('email', 'password');
    try {
      if (! $token = JWTAuth::attempt($credentials)) {
        return response()->json(['error' => 'Credenciales invalidas'], 400);
      }
      $user = User::where("email", $credentials["email"])->first();
    } catch (JWTException $e) {
      return response()->json(['error' => 'No se pudo crear el token, intente más tarde.'], 500);
    }
    return response()->json(compact('token', "user"));
  }

  public function getAuthenticatedUser()
  {
    try {
      if (!$user = JWTAuth::parseToken()->authenticate()) {
        return response()->json(['user_not_found'], 404);
      }
    } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
      return response()->json(['token_expired'], $e->getStatusCode());
    } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
      return response()->json(['token_invalid'], $e->getStatusCode());
    } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {
      return response()->json(['token_absent'], $e->getStatusCode());
    }
    return response()->json(compact('user'));
  }

  public function store(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users',
      'password' => 'required|string|min:6|confirmed',
    ]);

    if ($validator->fails()) {
      return redirect('register')
        ->withErrors($validator)
        ->withInput();
    }

    $user = User::create([
      'name' => $request->get('name'),
      'email' => $request->get('email'),
      'password' => Hash::make($request->get('password')),
      'rol_id' => $request->get("rol_id"),
    ]);

    return \Redirect::to('/users');
  }


  public function register(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'name' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users',
      'password' => 'required|string|min:6|confirmed',
    ]);

    if($validator->fails()){
      return response()->json($validator->errors()->toJson(), 400);
    }

    $user = User::create([
      'name' => $request->get('name'),
      'email' => $request->get('email'),
      'password' => Hash::make($request->get('password')),
      'rol_id' => $request->get("rol_id"),
    ]);

    $token = JWTAuth::fromUser($user);

    return response()->json(compact('user','token'),201);
  }

  public function refreshToken(){
    $token = JWTAuth::getToken();
    try{
      $token = JWTAuth::refresh($token);
      return response()->json(compact('token',),200);
    }catch (TokenExpiredException $ex){
      return response()->json(['error' => 'Inicie sesión de nuevo.'], 422);
    } catch (TokenBlacklistedException $ex){
      return response()->json(['error' => 'Inicie sesión de nuevo. (blacklisted)'], 422);
    }
  }

  public function logout(){
    $token = JWTAuth::getToken();
    try {
      $token = JWTAuth::invalidate($token);
      return response()->json(['success' => true, "message" => 'Sesión cerrada'], 200);
    }catch (JWTException $ex){
      return response()->json(['success' => false, "message" => 'No se pudo cerrar sesión'], 422);
    }
  }
}
