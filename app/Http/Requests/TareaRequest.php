<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TareaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          "nombre"=>"required|min:4",
          "descripcion"=>"required|min:4",
          "fecha_vencimiento"=>"required|date",
        ];
    }

  public function messages()
  {
    return [
      "nombre.*"=> "El nombre debe de tener al menos 4 carácteres.",
      "descripcion.*"=>"La descripción debe de tener al menos 4 carácteres.",
      "fecha_vencimiento.required"=>"La fecha de vencimiento es obligatoria.",
    ];
  }
}
