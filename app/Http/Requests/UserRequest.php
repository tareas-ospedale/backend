<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
  public function rules()
  {
    return [
      "name"=>"required|min:4",
      //"email"=>"required|unique:users,email,".Auth::user()->id,
      'email' => ['required', 'email', 'max:255', Rule::unique('users', "email")->ignore($usuario->id)],
    ];
  }

  public function messages()
  {
    return [
      "name.*"=> "El nombre debe de tener al menos 4 carácteres.",
      "email.*"=>"Este correo ya existe en el sistema.",
    ];
  }
}
