<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tarea extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $guarded = [];
    protected $fillable = [
      "nombre",
      "descripcion",
      "finalizada",
      "fecha_vencimiento",
      ];

    public function user(){
      return $this->belongsTo("App\Models\User","user_id");
    }
}
