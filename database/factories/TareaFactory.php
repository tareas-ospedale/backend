<?php

namespace Database\Factories;

use App\Models\Tarea;
use Illuminate\Database\Eloquent\Factories\Factory;

class TareaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Tarea::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
          'nombre' => $this->faker->name(),
          'descripcion' => $this->faker->paragraph,
          'finalizada' => 0,
          'fecha_vencimiento' => $this->faker->dateTimeBetween('+30 days', '+6 month'),
          'user_id' => 1,
        ];
    }
}
