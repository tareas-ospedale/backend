<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      \App\Models\User::factory(1)->create();
      \App\Models\Tarea::factory(60)->create();
      \App\Models\Rol::factory(1)->create();
      \App\Models\Rol::factory(1)->create(["nombre" => "Creador contenido"]);
    }
}
