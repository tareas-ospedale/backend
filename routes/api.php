<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TareaController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
//Route::get("/tareas",[App\Http\Controllers\TareaController::class, 'index'])->name("tareas");
Route::group(['middleware' => ['jwt.verify']], function() {
  Route::post('user','App\Http\Controllers\UserController@getAuthenticatedUser');
  Route::resource('tareas', 'App\Http\Controllers\TareaController');
  Route::post('refresh', 'App\Http\Controllers\UserController@refreshToken');
  Route::get('logout', 'App\Http\Controllers\UserController@logout');

});
Route::post("/auth/login","App\Http\Controllers\TokensController@login");
/*Route::group(["middleware" => ["jwt.auth"], "prefix" => "v1"],function(){
  Route::post("/auth/refresh","TokensController@refreshToken");
  Route::get("/auth/expire","TokensController@expire");
});*/
Route::post('register', 'App\Http\Controllers\UserController@register');
Route::post('login', 'App\Http\Controllers\UserController@authenticate');


//Route::get("consultar","App\Http\Controllers\TareaController@consultar")->name("tareas/consultar");

