<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TareaController;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  if(Auth::guest())
    return view('auth.login');
  else
    return view("tareas.index");

});

Route::get('/prueba', function () {
  var_dump(Auth::user()->esAdministrador());
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get("tareas/consultar","App\Http\Controllers\TareaController@consultar")->name("tareas/consultar");
Route::post("tareas/crear","App\Http\Controllers\TareaController@crear")->name("tareas/crear");
Route::post("tareas/borrar/{tarea_id}","App\Http\Controllers\TareaController@borrar")->name("tareas/borrar");
Route::post("tareas/actualizar/{tarea_id}","App\Http\Controllers\TareaController@actualizar")->name("tareas/actualizar");


Route::apiResource("tareas",TareaController::class);

Route::post("users/borrar/{tarea_id}","App\Http\Controllers\UserController@borrar")->name("users/borrar");
Route::post("users/actualizar-usuario/{usuario_id}","App\Http\Controllers\UserController@actualizarUsuario")->name("users/actualizar-usuario");
Route::get("users/consultar/{problema_controlador}","App\Http\Controllers\UserController@consultar")->name("users/consultar");

Route::apiResource("users",UserController::class);
