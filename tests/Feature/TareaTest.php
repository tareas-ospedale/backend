<?php

namespace Tests\Feature;

use App\Models\Tarea;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class TareaTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    public function api_is_accessible()
    {
      $this->json('get', 'api/tareas')
        ->assertStatus(200);
    }

    /** @test */
    public function a_user_can_read_all_the_tasks()
    {
      //Given we have task in the database
      $tarea = factory('App\TareaFactory')->create();

      //When user visit the tasks page
      $response = $this->get('/tareas');

      //He should be able to read the task
      $response->assertSee($tarea->nombre);
    }

    /** @test */
    public function a_post_can_be_created(){

      $this->withExceptionHandling();

      $response = $this->post('/tareas',[
          "nombre" => "Tarea 1",
          "descripcion" => "Completar esta semana",
          "finalizada" => 0,
          "fecha_vencimiento" => "2021-04-20",
          "user_id" => 1,
        ]
      );

      $response->assertOk();
      //$this->assertCount(1,Tarea::all());

      $tarea = Tarea::first();

      $this->assertEquals($tarea->nombre, "Tarea 1");
      $this->assertEquals($tarea->descripcion, "Completar esta semana");
      $this->assertEquals($tarea->finalizada, 0);
      $this->assertEquals($tarea->fecha_vencimietno, "2021-05-05");

    }
}
